jQuery(document).ready(function() {
	jQuery('.view-collection').on('click', function() {
        let cid = document.getElementById("collection").value;
        if(cid==-1)
        {
            showErrorMsg("Select a collection from the drop-down list to view items");
            return;
        }
        
        //let browse_url = 'index.php?option=com_collections&view=collections&layout=browse&cid=' + cid;
        let browse_url = 'collections?view=collections&layout=browse&cid=' + cid;
        browseCollection(browse_url);
    });

    let data_url = 'index.php?option=com_collections&task=loaditems';
    let itemsTbl = jQuery('#tbl_items').DataTable({
        "processing": true,
        "serverSide": true,
        "bFilter": true,
        "bInfo": true,
        "scrollX": false,    
        "pageLength": 10,
        "lengthMenu": [[5, 10, 25, 50, 100, 200, 500], [5, 10, 25, 50, 100, 200, 500]],
        "language": {
            processing: "Loading data..."
        },
        "ajax": {
            url: data_url,
            type: 'POST'
        }
    });

});

function browseCollection(browse_url){        
    jQuery.ajax({
        url: browse_url,
        type: "POST",
        success: function(data) {
           //console.log(data);
        },
        error: function(msg){
           //console.log(msg);
           let message = "Error occured while trying to load the collection";
           showErrorMsg(message);
        }
    });
}

function showErrorMsg(msg_txt) {
    //TODO - add error dialog
	alert(msg_txt);
}

function showInfoMsg(msg_txt) {
    //TODO - add info dialog
	alert(msg_txt);
}