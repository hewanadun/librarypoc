<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Collections Component Controller
 */
class CollectionsController extends JControllerLegacy
{
    public function display($cachable = false, $urlparams = false)
	{
        // Get the document object.
		$document = JFactory::getDocument();
        $format = $document->getType();

		// Set the default view name and format from the Request.
		$name = $this->input->getCmd('view', 'collections');
        $layout = $this->input->getCmd('layout', 'default');
        
		$this->input->set('view', $name);
        $this->input->set('layout', $layout);

		parent::display($cachable, $urlparams);

		return $this;
	}

    public function execute($task)
    {
        $task_func = strtolower($task) . 'Task';

        if (method_exists($this, $task_func)) {
            $this->$task_func();
            exit;
        } else {
            return parent::execute($task);
        }
    }

    public function loaditemsTask()
    {
        //error_log("loaditemsTask");
        global $conf;
        $records = DBHandler::aic_artworks_GetRecords("#__".$conf['collection']);
        print json_encode($records);
    }
}
