<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the Collections Component
 */
class CollectionsViewCollections extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		// Assign data to the view
		$this->title = "Art Institute of Chicago - Artworks Collection";
        $this->collections = DBHandler::getCollections();

		// Display the view
		parent::display($tpl);
	}
}