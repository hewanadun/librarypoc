<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

Utils::addCSS('collections.css');
Utils::addJS('collections.js');

?>

<!--//TODO - locally install the js and css files-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<h1><?php echo $this->title; ?></h1>
<br><br>
<div id ="div-collections" class="collectionsTable">
	
	<!-- Start Items Table -->
	<table id = 'tbl_items' class="dataTable display">
        <thead>
            <tr>
                <th>Title</th>
                <th>Artist</th>
                <th>Medium</th>
                <th>Categories</th>
                <th>Display Date(s)</th>
                <th>Place</th>
            </tr>
        </thead>

	</table>
</div>
