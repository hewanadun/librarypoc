<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

/**
 * Class for the definition of utility functions to be used throughout the 
 * component.
 */
class Utils 
{    
    public static function addCSS($file, $absolute = false){
        
        global $conf;

        // Define the absolute file path to the file
        $filePath = ($absolute) ? $file :
                    JPATH_BASE . "/" . $conf['css-dir'] . DS . $file;

        // Exit the function if the file path doesn't exist
        if ( ! file_exists($filePath)){
            error_log("Cannot find stylesheet: ".$filePath);
            return;
        }

        // Get the last time when the file was edited
        $fileModificationTime = filemtime($filePath);

        // Define the loading path for the given file
        $filePath = ($absolute) ? $file : $conf['css-dir'] . DS . $file;

        // Force the web page to grab the latest version by appending a version parameter to the file path. 
        // This avoids cache control in the browser
        $filePath .= '?v=' . $fileModificationTime;

        // Get the reference to the current document and add the file to the HTML page
        $document = Factory::getDocument();
        $document->addStylesheet($filePath);
    }

    public static function addJS($file, $absolute = false){
        
        global $conf;

        // Define the absolute file path to the file
        $filePath = ($absolute) ? $file :
                    JPATH_BASE . DS . $conf['js-dir'] . DS . $file;

        // Exit the function if the file path doesn't exist
        if ( ! file_exists($filePath)){
            error_log("Cannot find js file: ".$filePath);
            return;
        }

        // Get the last time when the file was edited
        $fileModificationTime = filemtime($filePath);

        // Define the loading path for the given file
        $filePath = ($absolute) ? $file : $conf['js-dir'] . DS . $file;

        // Force the web page to grab the latest version by appending a version parameter to the file path. 
        // This avoids cache control in the browser
        $filePath .= '?v=' . $fileModificationTime;

        // Get the reference to the current document and add the file to the HTML page
        $document = Factory::getDocument();
        $document->addScript($filePath);
    }
}