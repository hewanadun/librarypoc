<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Class for handling database queries
 */
class DBHandler 
{    
    public static function getCollections()
    {
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        // Select all records from the collections table.
        $query
            ->select($db->quoteName(array('cl.id', 'ins.name', 'cl.endpoint')))
            ->from($db->quoteName('#__collections', 'cl'))
            ->join('INNER', $db->quoteName('#__institutes', 'ins') . ' ON ' . $db->quoteName('cl.ins_id') . ' = ' . $db->quoteName('ins.id'))
            ->order($db->quoteName('ins.name'));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results
        $results = $db->loadAssocList();
        //error_log(print_r($results, TRUE));

        return $results;
    }

    public static function getCollection($cid)
    {
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        // Select the respective record from collections table.
        $query
            ->select($db->quoteName(array('cl.id', 'ins.alias', 'ins.name', 'ins.api_url', 'cl.endpoint')))
            ->from($db->quoteName('#__collections', 'cl'))
            ->join('INNER', $db->quoteName('#__institutes', 'ins') . ' ON ' . $db->quoteName('cl.ins_id') . ' = ' . $db->quoteName('ins.id'))
            ->where($db->quoteName('cl.id') . ' = ' . $db->quote($cid));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results
        $results = $db->loadAssoc();
        //error_log(print_r($results, TRUE));

        return $results;
    }


    public static function aic_artworks_GetRecords($table)
    {
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        /*
        id	int(11)	
        title	text	
        artist_title	varchar(128) NULL	
        artist_display	text NULL	
        place_of_origin	text NULL	
        date_display	text NULL	
        medium_display	text NULL	
        category_titles	text NULL	
        copyright_notice	text NULL	
        api_link	text NULL	
        image_id*/

        /*<th>Title</th>
        <th>Artist</th>
        <th>Medium</th>
        <th>Categories</th>
        <th>Display Date(s)</th>
        <th>Place</th>*/

        // Array of database columns which should be read and sent back to DataTables.
        $columns = array('title','artist_title','medium_display','category_titles','date_display','place_of_origin');
        
        // Select all records from the collections table.
        $query
            ->select($db->quoteName($columns))
            ->from($db->quoteName($table))
            ->setLimit(2);

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results
        $results = $db->loadAssocList();
        //error_log(print_r($results, TRUE));

        return $results;
    }

    public static function deleteAllRecords($table)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete($db->quoteName($table));

        $db->setQuery($query);

        $result = $db->execute();

        return $result;
    }
}