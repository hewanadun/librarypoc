<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

defined("DS") or define("DS", DIRECTORY_SEPARATOR);

// Include the configs file
require_once(__DIR__ . DS . 'configs.php');
//require_once(__DIR__ . '/helpers/ssp.class.php');

// Include the models
JLoader::register('DBHandler', __DIR__ . '/models/dbhandler.php');
JLoader::register('APIConnection', __DIR__ . '/models/apiconnection.php');

// Include the helper files
JLoader::register('Utils', __DIR__ . '/helpers/utils.php');

// Include JQuery & JQuery UI
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Collections
$controller = JControllerLegacy::getInstance('Collections');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();