<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_collections
 *
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

global $conf;

$conf['assets-dir'] = 'components/com_collections/assets';
$conf['js-dir'] = $conf['assets-dir'].'/js';
$conf['css-dir'] = $conf['assets-dir'].'/css';

$conf['page-limit'] = 100;
$conf['collection'] = 'aic_artworks';